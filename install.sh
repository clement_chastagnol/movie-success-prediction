#!/bin/bash

if test "$1" != "-f" -a $(echo $0 | grep install.sh > /dev/null; echo $?) -eq 0
then
    echo 'you must use `source ./install.sh`'
    exit 1
fi

test -z "$INTERPRETER" && INTERPRETER=`which python3`
test -z "$VENV" && VENV=./virtualenv
echo "Installing virtualenv in $VENV using interpreter $INTERPRETER"

if test ! -d $VENV
then
    echo "Setupping venv..."
    $INTERPRETER -m venv $VENV || return 1
    if test ! -f $VENV/bin/pip
    then
        ln -s `which pip` $VENV/bin/pip
    fi
    echo "Activating venv..."
    source $VENV/bin/activate
    pip install -U pip
    pip install wheel
else
    if test ! -f $VENV/bin/pip
    then
        echo "Missing pip: It seems you have a wrong version of python installed (<3.4)."
        return 1
    fi
fi

export VIRTUALENV=$venv

echo "Installing dependencies..."

mkdir ./wheels

pip wheel --wheel-dir ./wheels -r requirements.txt
pip install -r requirements.txt

echo OK
