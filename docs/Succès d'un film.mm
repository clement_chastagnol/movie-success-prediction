<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#000000" CREATED="1508437129322" ID="ID_248616658" MODIFIED="1508437580961" STYLE="fork" TEXT="Succ&#xe8;s d&apos;un film">
<font NAME="SansSerif" SIZE="20"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties"/>
<node COLOR="#0033ff" CREATED="1508437145068" ID="ID_745474510" MODIFIED="1508437580937" POSITION="right" TEXT="cast">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1508437367775" ID="ID_1821001936" MODIFIED="1509404263264" TEXT="acteurs">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1508437372598" ID="ID_207647388" MODIFIED="1508437580939" TEXT="identit&#xe9;">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1509800712075" ID="ID_569425016" MODIFIED="1509800716255" TEXT="nom, pr&#xe9;nom"/>
<node COLOR="#111111" CREATED="1509800716648" ID="ID_890013299" MODIFIED="1509800723805" TEXT="&#xe2;ge au tournage"/>
</node>
<node COLOR="#990000" CREATED="1508437377633" ID="ID_954326347" MODIFIED="1508437696781" TEXT="notori&#xe9;t&#xe9;">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1508437697511" ID="ID_1824010095" MODIFIED="1508437700246" TEXT="awardis&#xe9;?">
<node COLOR="#111111" CREATED="1508437446754" ID="ID_1715849154" MODIFIED="1508437580941" TEXT="comp&#xe9;tition mineure"/>
<node COLOR="#111111" CREATED="1508437457339" ID="ID_1667048576" MODIFIED="1508437713124" TEXT="comp&#xe9;tition majeure">
<node COLOR="#111111" CREATED="1508437462514" ID="ID_1087014901" MODIFIED="1508437580941" TEXT="c&#xe9;sar"/>
<node COLOR="#111111" CREATED="1508437464555" ID="ID_1006817356" MODIFIED="1508437580942" TEXT="palme d&apos;or"/>
<node COLOR="#111111" CREATED="1508437466555" ID="ID_892837287" MODIFIED="1508437580942" TEXT="oscar"/>
</node>
</node>
<node COLOR="#111111" CREATED="1508437395243" ID="ID_945375663" MODIFIED="1509404275529" TEXT="nb de film r&#xe9;alis&#xe9;s ant&#xe9;rieurement">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1508437734441" ID="ID_1797105964" MODIFIED="1508437745848" TEXT="visibilit&#xe9; r&#xe9;seaux sociaux"/>
</node>
<node COLOR="#990000" CREATED="1509800696729" ID="ID_451226760" MODIFIED="1509800699994" TEXT="r&#xf4;le film">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1508437152011" ID="ID_852742104" MODIFIED="1508444948691" POSITION="right" TEXT="production">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1508437161803" ID="ID_799187460" MODIFIED="1508444951087" TEXT="crew">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1508437268321" ID="ID_962797617" MODIFIED="1508437580946" TEXT="r&#xe9;alisateur">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1508437272207" ID="ID_881721855" MODIFIED="1508437580948" TEXT="sc&#xe9;nariste">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1508437275247" ID="ID_875544532" MODIFIED="1509404300692" TEXT="directeur de la photographie">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1508437278554" ID="ID_1043463560" MODIFIED="1508437580949" TEXT="compositeur">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1508437165648" ID="ID_1355232547" MODIFIED="1508437580950" TEXT="maison de prod">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1508437497562" ID="ID_720772135" MODIFIED="1508437580951" TEXT="nationalit&#xe9;">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1508437609903" ID="ID_1447395712" MODIFIED="1508437610926" TEXT="budget">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1508438111268" ID="ID_661480477" MODIFIED="1508438121736" TEXT="pays de tournage">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1508438405305" ID="ID_414097356" MODIFIED="1509404310217" TEXT="adaptation d&apos;un livre?">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1508437348935" ID="ID_1692217105" MODIFIED="1509800743164" POSITION="left" TEXT="caract&#xe9;ristiques">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1508437355355" ID="ID_442404405" MODIFIED="1508437580955" TEXT="genre">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1508437357683" ID="ID_1331886058" MODIFIED="1508437580956" TEXT="dur&#xe9;e">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1508437506808" ID="ID_1425193375" MODIFIED="1508437580958" TEXT="synopsis">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1508437510193" ID="ID_1015617605" MODIFIED="1508437580959" TEXT="script">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1508437519414" ID="ID_821151393" MODIFIED="1508437580961" TEXT="poster">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1508438509164" ID="ID_1218979706" MODIFIED="1508438512193" TEXT="language">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1508438615304" ID="ID_437432804" MODIFIED="1508438620595" TEXT="ratings">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1509404345445" ID="ID_857933535" MODIFIED="1509404349040" TEXT="boxOffice">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1508437800308" ID="ID_1054706760" MODIFIED="1508437811434" TEXT="saison sortie">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1508437803731" ID="ID_513665115" MODIFIED="1508437813401" TEXT="&#xe9;t&#xe9;">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1508437813708" ID="ID_1264974258" MODIFIED="1508437814858" TEXT="hiver">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1508437815077" ID="ID_1473390655" MODIFIED="1508437816471" TEXT="printemps">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1508437816710" ID="ID_187329544" MODIFIED="1508437818637" TEXT="automne">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
</node>
</node>
</map>
